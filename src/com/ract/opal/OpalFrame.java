/*
 * OpalFrame.java
 *
 * Created on __DATE__, __TIME__
 */

package com.ract.opal;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.ArrayList; //import java.awt.Frame;
import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author  __USER__
 */
public class OpalFrame extends javax.swing.JFrame {
	//ArrayList<OpalRequest> list = null;
	OpalClient[] list = null;

	/** Creates new form OpalFrame */
	public OpalFrame() {
		initComponents();
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		jLabel1 = new javax.swing.JLabel();
		jScrollBar1 = new javax.swing.JScrollBar();
		jButton1 = new javax.swing.JButton();
		jLabel2 = new javax.swing.JLabel();

		setTitle("OPAL");
		setBackground(new java.awt.Color(51, 255, 153));
		setForeground(new java.awt.Color(255, 255, 0));
		setLocationByPlatform(true);
		setResizable(false);
		setUndecorated(true);

		jLabel1.setBackground(new java.awt.Color(0, 255, 0));
		jLabel1.setFont(new java.awt.Font("Arial", 1, 18));
		jLabel1.setText("jLabel1");

		jScrollBar1
				.addAdjustmentListener(new java.awt.event.AdjustmentListener() {
					public void adjustmentValueChanged(
							java.awt.event.AdjustmentEvent evt) {
						adjustValueHandler(evt);
					}
				});

		jButton1.setFont(new java.awt.Font("Tahoma", 0, 14));
		jButton1.setText("X");
		jButton1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				hideHandler(evt);
			}
		});

		jLabel2.setFont(new java.awt.Font("Arial Black", 0, 12));
		jLabel2.setText("jLabel2");

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout
				.setHorizontalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								javax.swing.GroupLayout.Alignment.TRAILING,
								jPanel1Layout
										.createSequentialGroup()
										.addGap(20, 20, 20)
										.addComponent(
												jLabel1,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												698, Short.MAX_VALUE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(
												jScrollBar1,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												24,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(
												jLabel2,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												28,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(
												jButton1,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												43,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addContainerGap()));
		jPanel1Layout
				.setVerticalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																jScrollBar1,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																39,
																Short.MAX_VALUE)
														.addComponent(
																jLabel1,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																39,
																Short.MAX_VALUE)
														.addGroup(
																javax.swing.GroupLayout.Alignment.TRAILING,
																jPanel1Layout
																		.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.BASELINE)
																		.addComponent(
																				jButton1,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				38,
																				Short.MAX_VALUE)
																		.addComponent(
																				jLabel2,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				37,
																				Short.MAX_VALUE)))
										.addContainerGap()));

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE,
				javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE,
				javax.swing.GroupLayout.DEFAULT_SIZE,
				javax.swing.GroupLayout.PREFERRED_SIZE));

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	private void hideHandler(java.awt.event.ActionEvent evt) {
		this.setVisible(false);
	}

	int scrollBarValue = 0;

	private void adjustValueHandler(java.awt.event.AdjustmentEvent evt) {
		if (evt.getValue() > scrollBarValue) {
			scrollBarValue = evt.getValue();
			if (messageNo < numMessages - 1)
				messageNo++;
		} else if (evt.getValue() < scrollBarValue) {
			scrollBarValue = evt.getValue();
			if (messageNo > 0)
				messageNo--;
		}
		this.showAMessage(messageNo);
	}

	/**
	 * @param args the command line arguments
	 */
	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JButton jButton1;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JScrollBar jScrollBar1;
	// End of variables declaration//GEN-END:variables

	int schemeNo = 0;
	ArrayList<ColourScheme> cSchemes = new ArrayList<ColourScheme>();

	public void initColourSchemes() {
		ColourScheme cs = new ColourScheme(255, 255, 234, 238, 255, 238, 0,
				153, 0);
		cSchemes.add(cs);
		cs = new ColourScheme(255, 204, 0, 255, 153, 51, 153, 51, 0);
		cSchemes.add(cs);
		cs = new ColourScheme(195, 213, 228, 225, 238, 255, 0, 0, 255);
		cSchemes.add(cs);
		cs = new ColourScheme(228, 198, 195, 255, 235, 238, 255, 0, 0);
		cSchemes.add(cs);
		cs = new ColourScheme(206, 234, 212, 238, 255, 238, 0, 102, 51);
		cSchemes.add(cs);
		cs = new ColourScheme(238, 241, 221, 255, 255, 234, 255, 0, 0);
		cSchemes.add(cs);
		cs = new ColourScheme(255, 0, 0, 255, 255, 0, 0, 0, 0);
		cSchemes.add(cs);
		cs = new ColourScheme(255, 0, 255, 255, 255, 0, 0, 0, 0);
		cSchemes.add(cs);
		cs = new ColourScheme(0, 0, 255, 255, 0, 255, 255, 255, 0);
		cSchemes.add(cs);
		cs = new ColourScheme(0, 128, 255, 255, 255, 0, 255, 0, 0);
		cSchemes.add(cs);


	}

	public void setMessageText(String text) {
		this.jLabel1.setText(text);
	}

	int numMessages = 0;
	int messageNo = 0;
    ArrayList<String> messageLines = new ArrayList<String>(); 
	public void showMessages(OpalClient[] clist) {
		messageNo = 0;
		String[] mArray = null;
		String mLine = null;
		messageLines.clear();
		numMessages = 0;
		this.list = clist;
		for(int xx = 0;xx<clist.length;xx++)
		{
		   OpalClient clt = clist[xx];
//           System.out.println("\n " + clt.getMessageCount() + "  message = " + clt.getClientMessage() + "\n");			   
		   
		   if(clt.getMessageCount()!= null
			  && clt.getMessageCount().intValue()>1) 
		   {
			   mArray = clt.getClientMessage().split("\\|");
			   for(int yy = 0; yy<mArray.length;yy++)
			   {
					mLine = clt.getClientNo() + ": " + clt.getClientName().trim() + " - "
					      + mArray[yy];
					messageLines.add(mLine);
					numMessages++;
			   }
		   }
		   else
		   {
			   numMessages ++;
			   mLine =clt.getClientNo() + ": " + clt.getClientName().trim() + " - "
			         + clt.getClientMessage();
			   messageLines.add(mLine);
		   }
		}
		/*numMessages = this.list.length;*/
		if (numMessages <= 1) {
			this.jScrollBar1.setVisible(false);
			this.jLabel2.setVisible(false);
		} else {
			this.jScrollBar1.setVisible(true);
			this.jLabel2.setVisible(true);
		}
		showAMessage(0);
	}

	private void showAMessage(int messageNo) {
		/*OpalClient cl = list[messageNo];
		String ds = cl.getClientNo() + ": " + cl.getClientName().trim() + " - "
				+ cl.getClientMessage();*/
		this.jLabel1.setText((String) messageLines.get(messageNo));
		if (this.jLabel2.isVisible()) {
			String fr = "" + (messageNo + 1) + "/" + this.numMessages;
			this.jLabel2.setText(fr);
		}
	}

	public void showFrame(boolean show) {
		if (show) {
			this.setState(JFrame.NORMAL);
		} else {
			this.setState(JFrame.ICONIFIED);
		}
	}

	public void setColour() {
		schemeNo++;
		if (schemeNo >= cSchemes.size())
			schemeNo = 0;
		ColourScheme sc = cSchemes.get(schemeNo);
		sc.apply(jPanel1, jLabel1);
	}

	public void setPosition() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension frameSize = this.getSize();
		if (frameSize.height > screenSize.height) {
			frameSize.height = screenSize.height;
		}
		if (frameSize.width > screenSize.width) {
			frameSize.width = screenSize.width;
		}
		this.setLocation((screenSize.width - frameSize.width),
				(screenSize.height - frameSize.height - 30));
	}

	private int onClose() {
		return JFrame.HIDE_ON_CLOSE;
	}

	/* ***************** private class ColourScheme ********************* */
	private class ColourScheme {
		private Color outer;
		private Color inner;
		private Color labelColour;

		public ColourScheme(int r1, int r2, int r3, int r4, int r5, int r6,
				int r7, int r8, int r9) {
			outer = new Color(r1, r2, r3);
			inner = new Color(r4, r5, r6);
			labelColour = new Color(r7, r8, r9);
		}

		public void apply(JPanel panel, JLabel area) {
			panel.setBackground(outer);
			panel.setForeground(outer);
			area.setBackground(inner);
			area.setForeground(labelColour);
		}

	}

}
/* **************************** end of file ******************************* */