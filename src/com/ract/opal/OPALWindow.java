package com.ract.opal;

import java.io.BufferedInputStream;
//import java.io.StringReader;
//import java.rmi.RemoteException;
//import java.util.ArrayList;
import java.util.logging.Logger;


/** 
 * OPAL
 * @author dgk1
 * Class instantiated by client listener to handle
 * OPAL requests
 */
public class OPALWindow
{
    private static OpalFrame of;
	private Logger logger=null;
	
	public void read(BufferedInputStream inStream)throws Exception
	{
	    try
	    { 
	       java.io.Reader reader = new java.io.InputStreamReader(inStream);
	    
           OPAL op = OPAL.unmarshal(reader);
//           System.out.println(op.toString());
           showRequest(op);
	    }
	    catch(Exception ex)
	    {
	    	System.out.println(ex + "");
	    	ex.printStackTrace();
	    }
	}
	
	public void showRequest(OPAL op)
	{
	   OpalTransaction t = op.getTransaction();	
       if(of == null)
       {
		  of = new OpalFrame();
		  of.setVisible(false);
		  of.initColourSchemes();
       }
       if(t.getClientCount() == 0)
       {
    	   of.showFrame(false);
       }
       else
       {

    	//   of.setHeading("Product Opportunity Alert");	
	       of.setPosition();
		 //  of.setMessageText(op.getreq.getClientMessage());
		   of.showMessages(t.getClient());
		   of.setColour();
		   of.setVisible(true);
	 	   //of.setAlwaysOnTop(true);
		   of.showFrame(true);
		   of.toBack(); // lose focus
       }
    }
	
}
